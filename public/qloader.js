
var QLoader = {

  // @@jqueryUrl: URL for load Jquery
  // @@arrayScripts: Array of scripts to load
  // @@cbOnLoad: callback to ejecute after load
  load: function( jqueryUrl, arrayScripts, cbOnLoad ){

    this.createMessageDiv();

    // First, load jquery
    if( jqueryUrl ){
      var head  = document.getElementsByTagName('head')[0];
      var script= document.createElement('script');
      script.type= 'text/javascript';
      script.onreadystatechange= function () {
        if (this.readyState == 'complete'){
          setTimeout( function(){ QLoader.loadSecondPhase(arrayScripts, cbOnLoad) }, 500 );
        }
      }
      script.onload = function(){
        setTimeout( function(){ QLoader.loadSecondPhase(arrayScripts, cbOnLoad) }, 500 );
      };
      this.messageDiv.appendChild( document.createTextNode( 'Loading JQuery' ) );
      setTimeout( function(){ script.src = jqueryUrl; }, 300 );
      head.appendChild( script );
    }

  },

  loaded: false,
  finished: false,
  loadSecondPhase: function( arrayScripts, cbOnLoad ){
    this.messageDiv.innerHTML =  this.messageDiv.innerHTML + '.';
    try{
      var x = jQuery;
    } catch(e){
      this.jqueryTries --;
      if( this.jqueryTries == 0 ){
        this.messageDiv.innerHTML =  this.messageDiv.innerHTML + e;
      } else {
        setTimeout( function(){ QLoader.loadSecondPhase(arrayScripts, cbOnLoad) }, 500 );
      }
      return;
    }
    if( this.loaded ) return;
    this.loaded = true;
    var  len = arrayScripts.length;
    $(this.messageDiv).css( 'border', '2px solid black' )
      .css( 'background', 'transparent' )
      .css( 'width', this.messageWidth + 'px' )
      .css( 'height', this.messageHeight + 'px' )
      .css( 'position', 'absolute' )
      .css( 'text-align', 'center' )
      .css( 'font-size', this.messageFontSize + 'px' )
      .css( 'color', this.messageFontColor )
      .css( 'left', $(window).width() / 2  -  this.messageWidth / 2 )
      .css( 'top',  $(window).height() / 2 -  this.messageHeight / 2 );
    $(this.messageDiv).text( 'Loading ...' );
    $(this.messageDiv).append( $('<div />' )
      .css( 'background', this.messageBackgroundColor )
      .css( 'width', this.messageWidth + 'px' )
      .css( 'height', this.messageHeight + 'px' )
      .css( 'z-index', -2 )
          .css( 'left', '0px' )
          .css( 'top', '0px' )
      .css( 'position', 'absolute' ) );
    var q = 0;
    var bar = $( '<div />' )
          .css( 'background', this.messageBarColor )
          .css( 'z-index', -1 )
          .css( 'position', 'absolute' )
          .css( 'left', '0px' )
          .css( 'top', '0px' )
          .css( 'height', this.messageHeight )
          .css( 'width', '1px' );
    $(this.messageDiv).append( bar );

    var loadElement = function(src){
      q = q + 1;
      bar.animate( { width: ( QLoader.messageWidth * q / len ) + 'px' },
        function(){
          if( q == len ){
            if( QLoader.finished ) return;
            QLoader.finished = true;
            $(QLoader.messageDiv).fadeOut( 
              function(){ 
                if( cbOnLoad() ) cbOnLoad( );
                $(QLoader.messageDiv).remove(); 
              } );
          }
        } );
    }
    $.each( arrayScripts, function(i, src ){
      if( src.match( /\.css$/ ) ){
        $('head').append( $('<link rel="stylesheet" type="text/css" />')
              .attr('href', src)
            );
        // css link elements has not triggers load event.
        setTimeout( function(){ loadElement(src) }, 100 );
      } else if( src.match( /\.js$/ ) ){
        $.getScript( src )
            .done( function(){ loadElement(src) } )
            .fail( function(jqxhr, settings, exception){ 
              alert( 'Error on ' + src + ': ' + exception ) 
            } );
      } else {
        var img = $('<img />')
              .hide().attr( 'src', src );
        $('body').append( 
          img.load( function(){ loadElement(src); img.remove(); } ) );
      }
    } );
  },

  messageDiv: null,
  messageHeight: 16,
  messageWidth: 300,
  messageFontSize: 12,
  messageBackgroundColor: 'black',
  messageFontColor: 'white',
  messageBarColor: 'green',
  createMessageDiv: function(){
    var body  = document.getElementsByTagName('body')[0];
    this.messageDiv   = document.createElement('div');
    body.appendChild(this.messageDiv);
  },
  jqueryTries: 10  // 10 tries before die!
};
