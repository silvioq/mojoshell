function Shell(o) {
    var self = this;

    self.width = 120;
    self.height = 24;
    self._closed = false;

    var container = $('#container');
    container.html('');

    self.close = function() {
        self._closed = true;
        self.sendMessage( { "type": "close" } );
    };

    self.closed = function(){ return self._closed };

    self.maximizeSize = function(){
        var old = self.height;
        if( !self.fontWidth ){
            var dummy = $('<div class="row">x</div>').
                css( 'position', 'absolute' ).
                appendTo( 'body' );
            self.fontWidth = dummy.width();
            self.fontHeight = dummy.height();
            dummy.remove();
        }

        var h = $(window).height() - container.position().top - self.fontHeight * 1.5;
        var w = $(window).width() - container.position().left * 2 - self.fontWidth * 3;

        self.width  = parseInt(w / self.fontWidth);
        self.height = parseInt(h / self.fontHeight);
        container.width( self.width * self.fontWidth + self.fontWidth * 3 );
        container.height( self.height * self.fontHeight + self.fontHeight * 1.5 );
        self.sendMessage({"type":"resize","cols":self.width, "rows": self.height});

        var spaces = '';
        for (var i = 1; i <= self.width; i++) {
            spaces = spaces + "&nbsp;";
        }
        var shell = $('#shell' );
        var shellCursor = $('#shell-cursor' );
        var iconmenu    = $('#iconmenu' )
        iconmenu.position( { at: 'right-2 top+1', my: 'right top', of: shell } );
          
        if( old < self.height ){
            for (var i = old + 1; i <= self.height; i++) {
                shell.append('<div class="row" id="row' + i + '">'+spaces+'</div>');
                shellCursor.append('<div class="row-cursor" id="row-cursor' + i + '">'+spaces+'</div>');
            }
        } else if ( old > self.height ){
            for( var i = self.height + 1; i <= old; i ++ ){
                $('#row' + i ).remove();
                $('#row-cursor' + i ).remove();
            }
        }
    };

    self.init = function() {
        container.html('');

        container.append('<div id="shell"></div>');
        container.append('<div id="shell-cursor"></div>');

        var shell = $('#shell');
        var shellCursor = $('#shell-cursor');

        var spaces = '';
        for (var i = 1; i <= self.width; i++) {
            spaces = spaces + "&nbsp;";
        }

        for (var i = 1; i <= self.height; i++) {
            shell.append('<div class="row" id="row' + i + '">'+spaces+'</div>');
            shellCursor.append('<div class="row-cursor" id="row-cursor' + i + '">'+spaces+'</div>');
        }

        var iconmenu = $('<div id="iconmenu"></div>' )
                .css( 'position', 'absolute' );
        iconmenu.append('<div id="shell-close" class="ui-corner-all ui-state-default" title="Close connection"><span class="ui-icon ui-icon-close"></span></div>' );
        iconmenu.append('<div id="shell-refresh" class="ui-corner-all ui-state-default" title="Refresh"><span class="ui-icon ui-icon-refresh"></span></div>' );
        iconmenu.children()
                .css( 'padding', '3px' )
                .css( 'opacity', '0.3' )
                .css( 'margin-left', '2px' )
                .css( 'float', 'right' )
                .mouseenter( function(){
                   $(this).animate( { opacity: 1.0, queue: false } );
                } )
                .mouseleave( function(){
                   $(this).animate( { opacity: 0.3, queue: false } );
                } );
        container.append( iconmenu );
        iconmenu.position( { at: 'right-2 top+1', my: 'right top', of: shell } );

        self.bind();
        setTimeout( function(){ self.maximizeSize(); }, 1000 );
    };

    self.bind = function() {
        $(document).bind('keyup', function (e) {
            var code = e.keyCode || e.which;
            if (code == 27) {
                self.sendMessage({"type":"key","code":code});
            }
        });

        $(document).bind('keypress', function(e) {
            var code = e.keyCode || e.which;

            if (e.ctrlKey) {
                // Firefox
                if (code >= 97) {
                code -= 96;
                }

                self.sendMessage({"type":"key","code":code});
                return true;
            }

            // Pass arrows
            if (!e.shiftKey && e.keyCode && (code >= 37 && code <= 40)) {
                return false;
            }

            // Unicode?
            if (e.charCode > 128) {
                code = e.charCode;
            }

            self.sendMessage({"type":"key","code":code});

            return false;
        });

        $(document).keydown(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            // Enter and tab
            /* if (code == 8 || code == 9) {
                self.sendMessage({"type":"key","code":code});
                return false;
            } */

            var action;

            switch (code) {
                case 37: action = 'left'; break;
                case 38: action = 'up'; break;
                case 39: action = 'right'; break;
                case 40: action = 'down'; break;
                case 33: action = 'scrollup'; break;
                case 34: action = 'scrolldown'; break;
                default:
                  if( e.ctrlKey ) return false;
            }

            self.sendMessage({"type":"action","action":action});
            return false;
        });

        $(window).resize( function(){ 
            setTimeout( function(){ self.maximizeSize() }, 250 ) 
        } );

        $('#shell-refresh').click( function(){ self.sendMessage( { "type": "refresh" } ) } );
        $('#shell-close').click( function(){ self.close() } );

    };

    self.sendMessage = function (message) {
        self.onsend($.toJSON(message));
    };

    self.updateRow = function(n, data) {
        var row = $('#row' + n);
        row.html(data);
    };

    self.moveCursor = function(x, y) {
        var spaces = '';
        for (var i = 1; i <= self.width; i++) {
            spaces = spaces + " ";
        }

        var text = spaces;
        text = text.substr(0, x) + '|' + text.substr(x + 1);
        text = text.replace(/ /g, '&nbsp;');

        var row = $('#row-cursor' + y);
        row.html(text);
    };
}
