// vim: set cin sw=2:
var  MojoShell = {

  init: function( href ){
    var  ws;
    this.initui();
    try{
      ws = new WebSocket(href);
    }catch(e){
      ws = null;
    }
    if( ws ){
      ws.onopen = function(){
        MojoShell.log( "open" );
      };
      ws.onerror = function(evt){
        MojoShell.log( "error: " + evt.data );
      };
      ws.onmessage = function(evt){
        MojoShell.log( "msg: " + evt.data );
        var data = $.parseJSON( evt.data );
        if( data.type == 'row' ){
          shell.updateRow( data.row, data.text );
        } else if( data.type == 'cursor' ){
          shell.moveCursor( data.x, data.y );
        }
      };
      var shell = new Shell();
      ws.onclose = function(evt){
        MojoShell.log( "close" );
        if( !shell.closed() ){
          MojoShell.showErr( 'Shell closed from remote server' );
        } else {
          MojoShell.showInfo( 'Shell closed' );
        }
      };
      shell.onsend = function(msg){
        MojoShell.log( "send: " + msg );
        ws.send( msg );
      };
      shell.init();
    } else {
      MojoShell.showErr( 'Cant use Websocket. Please, update your browser.', true );
    };
    return ws;
  },

  initui: function(){
    this.logElement = $('#log');
    this.logElement.dialog( {
        modal: true,
        title: 'Log',
        autoOpen: false
        } );
    var le = this.logElement;
    $('#show-log').click(function(){ 
        le.dialog( 'option', 'width', $(document).width() - 20 );
        le.dialog( 'option', 'maxHeight', $(document).height() - 20 );
        le.dialog( 'open' ); 
    } );
    $('#congrats').fadeIn();
    var $error = $('#error');
    var $info  = $('#info');
    $error.css( 'z-index', 12 );
    var overlay = $('<div class="ui-overlay" id="overlay"><div class="ui-widget-overlay"></div></div>')
        .hide().appendTo('body');
    $('#error, #overlay, #info-msg').click( function(){
      if( $error.data( 'fatal' ) ) return;
      $error.fadeOut(function(){ $('#overlay').hide() } );
      $info.fadeOut( );
    } );
  },

  showErr: function(msg, fatal){
    $('#error-msg').text( msg );
    $('#error').position( { at: 'right-4 top+2', my: 'right top', of: window } ).fadeIn();
    if( fatal ) $('#error').data( 'fatal', true );
    $('#overlay').fadeIn();
  },

  showInfo: function(msg){
    $('#info-msg').text( msg );
    $('#info').position( { at: 'right-4 top+2', my: 'right top', of: window } ).fadeIn();
  },

  log: function(text){
    this.logElement.append( text + "<br />" );
  }
}
